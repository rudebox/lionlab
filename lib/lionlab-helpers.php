<?php
 /**
   * Description: Lionlab helpers
   *
   * @package Lionlab
   * @subpackage Lionlab
   * @since Version 1.0
   * @author Kaspar Rudbech
   */

	//remove countrycode from phone
	function get_formatted_phone($str) {
	  
	  // Remove +45
	  $str = str_replace('+', '00', $str);

	  // Only allow integers
	  $str = preg_replace('/[^0-9]/s', '', $str);

	  return $str;
	}


	//allow svg uploads
	function cc_mime_types($mimes) {
	  $mimes['svg'] = 'image/svg+xml';
	  return $mimes;
	}
	add_filter('upload_mimes', 'cc_mime_types');


	// Custom excerpt length
	function custom_excerpt_length( $length ) {
	  return 20;
	}

	add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );


	// Custom excerpt text
	function custom_excerpt_more( $more ) {
	  return '&hellip;';
	}

	add_filter('excerpt_more', 'custom_excerpt_more');


	// Move Yoast to bottom
	function yoasttobottom() {
	  return 'low';
	}
	add_filter( 'wpseo_metabox_prio', 'yoasttobottom');


	//get proper title
	function get_proper_title( $id = null  ) {
	  
	  if ( is_null($id) ) {
	    global $post;
	    $id = $post->ID;
	  }

	  $acf_title = get_field('page_title', $id);

	  return ($acf_title) ? $acf_title : get_the_title( $id );
	}
	

	//Returns pagination element with posibility of using a custom query
	function lionlab_pagination_hook( $query ) {

	  // Fallback to global query
	  if ($query == null) {
	    global $wp_query;
	    $query = $wp_query;
	  }

	  // Need an unlikely integer
	  $big = 999999999;

	  // Arguments
	  $args = array(
	    'base'               => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
	    'format'             => '?paged=%#%',
	    'total'              => $query->max_num_pages,
	    'current'            => max( 1, get_query_var('paged') ),
	    'show_all'           => false,
	    'end_size'           => 1,
	    'mid_size'           => 1,
	    'prev_next'          => false,
	    'type'               => 'plain',
	  );

	  // Only display if more than 1 page
	  if ( intval($query->max_num_pages) > 1 ) {
	    ob_start() ?>
	    
	    <div class="pagination flex flex--valign">
	      <div class="pagination__nav pagination__previous left"><?php previous_posts_link('<i class="fas fa-angle-left"></i>'); ?></div>
	    
	        <div class="pagination__pages"><?php echo paginate_links($args); ?></div>

	      <div class="pagination__nav pagination__next right"><?php next_posts_link('<i class="fas fa-angle-right"></i>'); ?></div>
	    </div>

	    <?php
	    echo ob_get_clean();
	  }
	}

	add_action( 'lionlab_pagination', 'lionlab_pagination_hook' );


?>