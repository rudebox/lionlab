<?php 
    // Register Custom Post Type Case
    function create_case_cpt() {

      $labels = array(
        'name' => _x( 'Cases', 'Post Type General Name', 'lionlab' ),
        'singular_name' => _x( 'Case', 'Post Type Singular Name', 'lionlab' ),
        'menu_name' => _x( 'Cases', 'Admin Menu text', 'lionlab' ),
        'name_admin_bar' => _x( 'Case', 'Add New on Toolbar', 'lionlab' ),
        'archives' => __( 'Case Archives', 'lionlab' ),
        'attributes' => __( 'Case Attributes', 'lionlab' ),
        'parent_item_colon' => __( 'Parent Case:', 'lionlab' ),
        'all_items' => __( 'Alle Cases', 'lionlab' ),
        'add_new_item' => __( 'Tilføj ny Case', 'lionlab' ),
        'add_new' => __( 'Tilføj ny', 'lionlab' ),
        'new_item' => __( 'Ny Case', 'lionlab' ),
        'edit_item' => __( 'Rediger Case', 'lionlab' ),
        'update_item' => __( 'Update Case', 'lionlab' ),
        'view_item' => __( 'Se Case', 'lionlab' ),
        'view_items' => __( 'Se Cases', 'lionlab' ),
        'search_items' => __( 'Søg Case', 'lionlab' ),
        'not_found' => __( 'Not found', 'lionlab' ),
        'not_found_in_trash' => __( 'Not found in Trash', 'lionlab' ),
        'featured_image' => __( 'Featured Image', 'lionlab' ),
        'set_featured_image' => __( 'Set featured image', 'lionlab' ),
        'remove_featured_image' => __( 'Remove featured image', 'lionlab' ),
        'use_featured_image' => __( 'Use as featured image', 'lionlab' ),
        'insert_into_item' => __( 'Insert into Case', 'lionlab' ),
        'uploaded_to_this_item' => __( 'Uploaded to this Case', 'lionlab' ),
        'items_list' => __( 'Cases list', 'lionlab' ),
        'items_list_navigation' => __( 'Cases list navigation', 'lionlab' ),
        'filter_items_list' => __( 'Filter Cases list', 'lionlab' ),
      );
      $rewrite = array(
        'slug' => 'referencer',
        'with_front' => true,
        'pages' => true,
        'feeds' => true,
      );
      $args = array(
        'label' => __( 'Case', 'lionlab' ),
        'description' => __( '', 'lionlab' ),
        'labels' => $labels,
        'menu_icon' => 'dashicons-archive',
        'supports' => array('title', 'editor', 'thumbnail', 'revisions', 'author', 'page-attributes', 'post-formats'),
        'taxonomies' => array('category', 'cases'),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 5,
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'can_export' => true,
        'has_archive' => true,
        'hierarchical' => false,
        'exclude_from_search' => false,
        'show_in_rest' => true,
        'publicly_queryable' => true,
        'capability_type' => 'post',
        'rewrite' => $rewrite,
      );
      register_post_type( 'case', $args );

    }
    add_action( 'init', 'create_case_cpt', 0 );
 ?>