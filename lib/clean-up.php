<?php 
  /**
   * Description: Lionlab clean up redundant WP classes and such
   *
   * @package Lionlab
   * @subpackage Lionlab
   * @since Version 1.0
   * @author Kaspar Rudbech
   */

  //Body classes
  function lionlab_body_classes($classes) {

    // Browser specific classes based on user agent globals
    global $is_gecko, $is_IE, $is_opera, $is_safari, $is_chrome, $is_edge, $is_iphone, $post;

    if ($is_gecko)          $classes[] = 'is-gecko';
    elseif ($is_opera)      $classes[] = 'is-opera';
    elseif ($is_iphone)       $classes[] = 'is-iphone';
    elseif ($is_safari)     $classes[] = 'is-safari';
    elseif ($is_chrome)     $classes[] = 'is-chrome';
    elseif ($is_IE)         $classes[] = 'is-ie';
    elseif ($is_edge)       $classes[] = 'is-edge';
    else                $classes[] = 'is-unknown';

    // WPML language
    if (function_exists('icl_object_id')) $classes[] = 'wpml-' . ICL_LANGUAGE_CODE;
    
    // Remove unnecessary classes
    $page_id_class = 'page-id-' . get_the_ID();
    $parent_id_class = 'parent-pageid-' . get_the_ID();
    $post_id_class = 'postid-' . get_the_ID();
    $remove_classes = array(
      'page-template-default', 'page-template', 'page-template-page-layouts', 'page-template-page-layouts-php', $parent_id_class, $page_id_class, $post_id_class, 'page-layouts', 
      'page', 'page-parent', 'post-type-archive', 'page-template-contact-template' ,'page-template-parts', 'page-template-partscontact-template-php' ,'post-template-default', 'single-format-standard', 'page-child', 'ads', 'google-ads', 'adwords', 'google-adwords'
    );
    
    $classes = array_diff($classes, $remove_classes);
    
    return $classes;
  }

  add_filter('body_class', 'lionlab_body_classes');


  //remove emoji, embed scripts and styling for performance optimization
  remove_action( 'wp_head', 'print_emoji_detection_script', 7 ); 
  remove_action( 'admin_print_scripts', 'print_emoji_detection_script' ); 
  remove_action( 'wp_print_styles', 'print_emoji_styles' ); 
  remove_action( 'admin_print_styles', 'print_emoji_styles' );

  //remove necessary wp-embed js file
  function my_deregister_scripts(){
      wp_deregister_script( 'wp-embed' );
  }

  add_action( 'wp_footer', 'my_deregister_scripts' );


  add_filter( 'wp_default_scripts', 'dequeue_jquery_migrate' );

  function dequeue_jquery_migrate( &$scripts){
    if(!is_admin()){
      $scripts->remove( 'jquery');
      $scripts->add( 'jquery', false, array( 'jquery-core' ), '1.10.2' );
    }
  }

  // Clean up wordpres <head>
  remove_action('wp_head', 'rsd_link'); // remove really simple discovery link
  remove_action('wp_head', 'wp_generator'); // remove wordpress version
  remove_action('wp_head', 'feed_links', 2); // remove rss feed links (make sure you add them in yourself if youre using feedblitz or an rss service)
  remove_action('wp_head', 'feed_links_extra', 3); // removes all extra rss feed links
  remove_action('wp_head', 'index_rel_link'); // remove link to index page
  remove_action('wp_head', 'wlwmanifest_link'); // remove wlwmanifest.xml (needed to support windows live writer)
  remove_action('wp_head', 'start_post_rel_link', 10, 0); // remove random post link
  remove_action('wp_head', 'parent_post_rel_link', 10, 0); // remove parent post link
  remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0); // remove the next and previous post links
  remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
  remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);

  //remove gutenberg css
  function custom_theme_assets() {
    wp_dequeue_style( 'wp-block-library' );
  }

  add_action( 'wp_enqueue_scripts', 'custom_theme_assets', 100 );

  //gravity form scripts in footer
  add_filter("gform_init_scripts_footer", "init_scripts");
    function init_scripts() {
    return true;
  }

?>