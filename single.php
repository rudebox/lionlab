<?php get_template_part('parts/header'); the_post(); ?>

<main>

  <?php get_template_part('parts/page', 'header');?>

  <?php   
    //post bg
    $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'url' ); 

    //blog meta
    $author = get_the_author(); 

    $author_id = get_the_author_meta('ID');
    $avatar = get_field('avatar', 'user_'. $author_id );
  ?>

  <div class="page__img-container wrap hpad">
    <div class="page__img--wrapper">
      <div class="page__img is-animated is-animated--zoomIn" style="background-image: url(<?php echo esc_url($thumb[0]); ?>);"></div>
    </div>

    <div class="single__meta">
        
        <div class="single__meta-wrap flex">
          <img class="single__avatar" src="<?php echo esc_attr($avatar['sizes']['thumbnail']); ?>" alt="<?php echo esc_html($author); ?>">

          <div class="single__meta-wrap">
            <h6 class="single__name"><?php echo esc_html($author); ?></h6>

            <?php if (get_the_author_meta('description') ) : ?>
              <span class="single__position"><?php echo (get_the_author_meta('description') ); ?></span>
            <?php endif; ?>
          </div>

        </div>

    </div>
  </div>


  <section class="single padding--bottom">
    <div class="wrap hpad">
      <div class="row">

        <article class="col-sm-8 col-sm-offset-2" itemscope itemtype="http://schema.org/BlogPosting">

          <div itemprop="articleBody">
            <?php the_content(); ?>
          </div>

        </article>

      </div>
    </div>
  </section>

</main>

<?php get_template_part('parts/footer'); ?>