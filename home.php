<?php get_template_part('parts/header'); ?>

<main>

<?php get_template_part('parts/page', 'header'); ?>

  <section class="blog padding--bottom">
    <div class="wrap hpad">
      <?php get_template_part('parts/filter'); ?>
      <div class="row mixit flex flex--wrap">

        <?php if (have_posts()): ?>
          <?php while (have_posts()): the_post(); ?>

            <?php 
                $cats = get_the_category();
                $cat_string = "";

                foreach ($cats as $cat) {
                  $cat_string .= " cat" . $cat->term_id ."";
                }
            ?>

            <?php   
              //post img
              $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'url' ); 
            ?>

            <a class="blog__post mix <?php echo esc_attr($cat_string); ?>" href="<?php the_permalink(); ?>" itemscope itemtype="http://schema.org/BlogPosting" style="background-image: url(<?php echo esc_url($thumb[0]); ?>)">

              <header>
                <h2 class="blog__title h4" itemprop="headline" title="<?php the_title_attribute(); ?>">
                    <?php the_title(); ?>
                </h2>
              </header>

            </a>

          <?php endwhile; else: ?>

            <p>Ingen indlæg i skoene eller i denne kategori for den sags skyld.</p>

        <?php endif; ?>

      </div>

      <?php 
        do_action( 'lionlab_pagination' );
      ?>
    </div>
  </section>

  <?php get_template_part('parts/contact'); ?>

</main>

<?php get_template_part('parts/footer'); ?>