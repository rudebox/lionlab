<?php
if( have_rows('layout') ) {
  $GLOBALS['layout_count'] = 1; while ( have_rows('layout') ) { the_row();
    if( get_row_layout() === 'employees' ) { ?>

      <?php get_template_part( 'parts/layouts/layout', 'employees' ); ?>

    <?php
    } elseif( get_row_layout() === 'references' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'references' ); ?>

    <?php
    } elseif( get_row_layout() === 'tags' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'tags' ); ?>

    <?php
    } elseif( get_row_layout() === 'selected-cases' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'cases' ); ?>

    <?php
    } elseif( get_row_layout() === 'services' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'services' ); ?>

    <?php
    } elseif( get_row_layout() === 'video' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'video' ); ?>

    <?php
    } elseif( get_row_layout() === 'client-results' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'client-results' ); ?> 

    <?php
    } elseif( get_row_layout() === 'wysiwygs' ) {
    ?>

      <?php get_template_part( 'parts/layouts/layout', 'wysiwygs' ); ?>

    <?php
    }
    ?>

  <?php
    $GLOBALS['layout_count']++;
  }
  ?>

<?php
} else {
?>
  <p class="center">You haven't added any layouts yet. <?php edit_post_link('Add one now.'); ?></p>
<?php
}
?>
