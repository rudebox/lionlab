<?php
	$category_name = 'blog';
	$category = get_category_by_slug( $category_name );
	$category_id = $category->term_id;

	$args = array('child_of' => $category_id);
	$categories = get_categories( $args );
?>

<div class="blog__controls flex flex--justify flex--wrap">

  <div class="blog__filter--dropdown h5">Filtrer <?php echo file_get_contents('wp-content/themes/lionlab/assets/img/arrow-dropdown.svg'); ?></div>

  <div class="blog__filter-wrap">
    <div class="blog__filter blog__filter--all" data-filter="all">Alle</div>

    <?php foreach($categories as $category) : ?>
      <div class="blog__filter" data-filter=".cat<?php echo $category->term_id;?>"><?php echo $category->name; ?></div> 
    <?php endforeach; ?>
  </div>

</div>