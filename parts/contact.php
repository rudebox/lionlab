<?php 
/**
* Description: Lionlab contact field group options page
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/


$img = get_field('form_img', 'options');
$title = get_field('form_title', 'options');
$text = get_field('form_text', 'options');
$form_id = get_field('form_id', 'options');
?>

<?php if (is_front_page() ) : ?>
<section class="contact--img padding--top">
	<div class="wrap hpad flex">
		<img src="<?php echo esc_url($img['sizes']['featured_img']); ?>" alt="<?php echo $img['alt']; ?>">
	</div>
</section>
<?php endif; ?>

<section class="contact padding--both bg--grey">
	<div class="wrap hpad">
		
		<div class="center">
			<h2><?php echo esc_html($title); ?></h2>
			<?php echo $text; ?>
			<?php gravity_form( $form_id, $display_title = false, $display_description = false, $display_inactive = false, $field_values = null, $ajax = false, 2, $echo = true ); ?>
		</div>

	</div>
</section>