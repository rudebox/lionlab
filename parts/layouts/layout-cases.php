<?php 
  //sections settings
  $margin = get_sub_field('margin');
?>

 <section class="cases padding--<?php echo esc_attr($margin); ?>">

    <div class="wrap hpad">
      <div class="row flex flex--wrap masonry">
        
          <?php 
            //counter
            $i=0;

            $cases = get_sub_field('selected_case');

            //query arguments
            $args = array(
              'posts_per_page' => -1,
              'post_type' => 'case',
              'post__in' => $cases
            );
             
            $query = new WP_QUERY($args);
            
           ?>

          <?php if ($query->have_posts()): ?>
            <?php while ($query->have_posts()): $query->the_post(); ?>


            <?php   
              //get thumb
              $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'url' );
              //post img alt tag
              $alt = get_post_meta($thumb, '_wp_attachment_image_alt', true); 

              //get categories
              $categories = get_the_category();

              $title = get_field('page_title');
              $color = get_field('case_color');

              $reference_text = get_sub_field('slected_case_referene_text');
              $reference_rating = get_sub_field('slected_case_referene_rating');

              if ($reference_rating === '1') {
                $reference_rating = '<div class="cases__rating"><i class="fas fa-star"></i></div>';
              } 

              if ($reference_rating === '2') {
                $reference_rating = '<div class="cases__rating"><i class="fas fa-star"></i><i class="fas fa-star"></i></div>';
              } 

              if ($reference_rating === '3') {
                $reference_rating = '<div class="cases__rating"><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i></div>';
              } 

              if ($reference_rating === '4') {
                $reference_rating = '<div class="cases__rating"><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i></div>';
              } 

              if ($reference_rating === '5') {
                $reference_rating = '<div class="cases__rating"><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i></div>';
              } 

              $i++;

                //insert reference
                if ($i === 4) {
                  echo '<div class="cases__reference col-sm-6">' . $reference_text . '';
                  echo '<div class="cases__rating yellow">' . $reference_rating . '';

                }

                if ($i === 4) { echo '</div></div>'; }
            ?>

             <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" class="cases__item col-sm-6" itemscope itemtype="http://schema.org/BlogPosting" style="background-image: url(<?php echo esc_url($thumb[0]); ?>)">
                
                <div class="cases__wrap flex flex--center flex--justify flex--wrap">
                  <header>
                    <h2 class="cases__title--meta <?php echo esc_attr($color); ?> h4">                   
                        <?php the_title(); ?>
                    </h2>

                    <h2 itemprop="headline" class="cases__title h3"><?php echo esc_html($title); ?></h2>
                  </header>
                </div>

              </a>

            <?php endwhile; wp_reset_postdata(); else: ?>
              
              <p>Cases er ikke tilgængelige på nuværende tidspunkt.</p>

          <?php endif; ?>

      </div>
    </div>
  </section>