<?php 
  //section settings
  $margin = get_sub_field('margin');
  $bg = get_sub_field('bg');

  //video 
  $video_teaser = get_sub_field('video_teaser');
  $video_mp4 = get_sub_field('video_mp4');
  $video_ogv = get_sub_field('video_ogv');
  $video_webm = get_sub_field('video_webm');
 ?>

<section class="video padding--<?php echo esc_attr($margin); ?> bg--<?php echo esc_attr($bg); ?>">
	<div class="wrap--fluid">
		<div class="row flex flex--hvalign">
			<div class="col-sm-12 flex flex--hvalign video__wrap">

				<video class="video__video video__video--preview" preload="auto" autoplay loop muted="muted" volume="0">
					<source src="<?php echo esc_url($video_teaser); ?>" type="video/mp4" codecs="avc1, mp4a">
					Your browser doesn't support HTML5 video. upgrade your browser to improve your experience.
				</video>

				<video id="player" class="video__video video__video--full-length" preload="auto" loop muted="muted" volume="0">
					<source src="<?php echo esc_url($video_mp4); ?>" type="video/mp4" codecs="avc1, mp4a">
					<source src="<?php echo esc_url($video_ogv); ?>" type="video/ogg" codecs="theora, vorbis">
					<source src="<?php echo esc_url($video_webm); ?>" type="video/webm" codecs="vp8, vorbis">
					Your browser doesn't support HTML5 video. upgrade your browser to improve your experience.
				</video>

				<a class="video__controls no-ajax" data-fancybox href="<?php echo esc_url($video_file); ?>">
			    	<div class="video__play"><?php echo file_get_contents('wp-content/themes/lionlab/assets/img/play.svg'); ?> </div>
				</a>

			</div>

		</div>
	</div>
</section>