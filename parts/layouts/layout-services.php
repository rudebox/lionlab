<?php 
/**
* Description: Lionlab services repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$margin = get_sub_field('margin');

if (have_rows('service') ) :
?>

<section class="services padding--<?php echo esc_attr($margin); ?>">
	<div class="wrap hpad">

		<div class="row flex flex--wrap">
			<?php while (have_rows('service') ) : the_row(); 
				$name = get_sub_field('service_name');
				$text = get_sub_field('service_text');
			?>

			<div class="col-sm-6 bg--grey services__item is-animated is-animated--fadeUp">		
				<h2 class="services__title"><?php echo esc_html($name); ?></h2>
				<p class="services__text"><?php echo esc_html($text); ?></p>

				<?php if (have_rows('service_repeater') ) : while (have_rows('service_repeater') ) : the_row();
					$sub_name = get_sub_field('service_sub_name');
					$sub_link = get_sub_field('service_sub_link');
				?>
					<a class="btn btn--tag btn--black" href="<?php echo esc_url($sub_link); ?>"><?php echo esc_html($sub_name); ?></a>
				<?php endwhile; endif; ?>
			</div>
			<?php endwhile; ?>
		</div>

	</div>
</section>
<?php endif; ?>