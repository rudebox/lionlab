<?php 
/**
* Description: Lionlab services repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$margin = get_sub_field('margin');

if (have_rows('clients_results') ) :
?>

<section class="client-results padding--<?php echo esc_attr($margin); ?>">
	<div class="wrap hpad">

		<div class="row--fluid flex flex--wrap">
			<?php while (have_rows('clients_results') ) : the_row(); 
				$logo = get_sub_field('logo');
				$name = get_sub_field('name');
				$services = get_sub_field('services');
				$text = get_sub_field('text');
				$result = get_sub_field('result');
			?>

			<div class="col-sm-12 client-results__item flex flex--center is-animated is-animated--fadeUp">	
 				<div class="client-results__img">
					<img class="" src="<?php echo esc_url($logo['sizes']['thumbnail']); ?>" alt="<?php echo esc_attr($logo['alt']); ?>">
				</div>

				<h6 class="client-results__title"><?php echo esc_html($name); ?></h6>

	<!-- 			<div class="client-results__btns">				
				<?php 				
					if ($services['google_ads'] === true) {
						echo  '<span class="client-results__btn btn--purple">Google ads</span>';
					} 
					if ($services['some'] === true) {
						echo '<span class="client-results__btn btn--blue">Facebook</span>';
					}
					if ($services['website'] === true) {
						echo '<span class="client-results__btn btn--orange">Website</span>';
					}
				?>
				</div> -->

				<p class="client-results__text"><?php echo esc_html($text); ?></p>

				<div class="client-results__result"><?php echo esc_html($result); ?> <?php echo file_get_contents(get_template_directory_uri() . '/assets/img/polygon.svg'); ?></div>

			</div>
			<?php endwhile; ?>
		</div>

	</div>
</section>
<?php endif; ?>