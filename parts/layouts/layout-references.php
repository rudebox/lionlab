<?php 
/**
* Description: Lionlab references repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$margin = get_sub_field('margin');

if (have_rows('reference') ) :
?>

<section class="references padding--<?php echo esc_attr($margin); ?>">
	<div class="wrap--fuid references__container">
		<div class="flex flex--center">
			<?php while (have_rows('reference') ) : the_row(); 
				$logo = get_sub_field('logo');
			?>

			<div class="references__item">				
				<img class="references__img" src="<?php echo esc_url($logo['url']); ?>" alt="<?php echo esc_attr($logo['alt']); ?>">
			</div>
			<?php endwhile; ?>
		</div>
	</div>
</section>
<?php endif; ?>