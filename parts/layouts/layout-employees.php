<?php 
/**
* Description: Lionlab employees repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');

if (have_rows('employee') ) :

if (is_page_template('parts/contact-template.php') ) {
	$class = 'employee--contact';
}
?>

<section class="employee <?php echo esc_attr($class); ?> padding--<?php echo esc_attr($margin); ?>">
	<div class="wrap hpad">

		<div class="row flex flex--wrap">
			<?php while (have_rows('employee') ) : the_row(); 
				$name = get_sub_field('employee_name');
				$img = get_sub_field('employee_img');
				$mail = get_sub_field('employee_mail');
				$phone = get_sub_field('employee_phone');
				$position = get_sub_field('employee_position');
			?>

			<div class="col-sm-3 employee__item is-animated is-animated--fadeUp">		
				<img src="<?php echo esc_url($img['sizes']['employee']); ?>" alt="<?php echo esc_attr($img['alt']); ?>">
				<p class="employee__position"><?php echo $position; ?></p>
				<h4 class="employee__name"><?php echo esc_html($name); ?></h4>
				<a class="employee__link no-ajax" href="mailto:<?php echo esc_html($mail); ?>"><?php echo esc_html($mail); ?></a><br>
				<a class="employee__link no-ajax" href="tel:<?php echo esc_html(get_formatted_phone($phone)); ?>"><?php echo esc_html($phone); ?></a>
				
			</div>
			<?php endwhile; ?>
		</div>

	</div>
</section>
<?php endif; ?>