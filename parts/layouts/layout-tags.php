<?php 
/**
* Description: Lionlab tags repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$margin = get_sub_field('margin');

?>

<section class="tags padding--<?php echo esc_attr($margin); ?>">
	<div class="wrap hpad">
		<div class="row flex flex--valign flex--wrap">
			<?php 
				$tags = get_sub_field('tag_link');

				foreach ($tags as $tag) :
			?>

				<a href="<?php echo the_permalink($tag->ID); ?>" class="btn btn--blue btn--tag">
					<?php echo $tag->post_title; ?>
				</a>

			<?php endforeach; ?>

		</div>
	</div>
</section>