<?php 
	if ( is_tax() ) {
		$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
		$title = $term->name;
	} elseif ( is_category() ) {
		$title = single_cat_title( '', false );
	} elseif ( is_archive() ) {
		$title = post_type_archive_title( '', false );
	} elseif ( is_404() ) {
		$title = __('Siden kunne ikke findes', 'lionlab');
	} elseif ( is_search() ) {
		$title = __('Søgeresultat', 'lionlab');
	} else {
		$id = (is_home()) ? get_option('page_for_posts') : $post->ID;
		$title = get_proper_title($id);
	}

	//pageheader variables
	$page_img = get_field('page_img');
	$page_bg = get_field('page_bg');


	//post bg
	$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'url' ); 

	//check if is template
	if (is_page_template('parts/contact-template.php') || is_singular('case') ) {
		$class = 'page__hero--intersect';
	}	
?>



<section class="page__hero <?php echo esc_attr($class); ?> bg--<?php echo esc_attr($page_bg); ?>">
	<div class="wrap hpad page__container">
		<div class="row">

			<div class="col-sm-6 col-sm-offset-1">
				<h1 class="page__title">
					<?php echo $title; ?> 
					<?php if (is_singular('case') ) : ?>
						<span><?php echo the_title(); ?></span>
					<?php endif; ?>
				</h1>
				<?php echo file_get_contents('wp-content/themes/lionlab/assets/img/lion.svg'); ?>
			</div>

		</div>
	</div>
</section>