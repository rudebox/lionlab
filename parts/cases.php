<?php 
  //sections settings
  $margin = get_sub_field('margin');
?>

 <section class="cases cases--overlay padding--both">

    <div class="wrap hpad">
      <div class="row flex flex--wrap">
        
          <?php 

            $cases = get_field('cases_pick');

            //query arguments
            $args = array(
              'posts_per_page' => 2,
              'post_type' => 'case',
              'post__in' => $cases
            );
             
            $query = new WP_QUERY($args);
            
           ?>

          <?php if ($query->have_posts()): ?>
            <?php while ($query->have_posts()): $query->the_post(); ?>


            <?php   
              //get thumb
              $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'url' );
              //post img alt tag
              $alt = get_post_meta($thumb, '_wp_attachment_image_alt', true); 

              //get categories
              $categories = get_the_category();

              $title = get_field('page_title');
            ?>

             <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" class="cases__item col-sm-6" itemscope itemtype="http://schema.org/BlogPosting" style="background-image: url(<?php echo esc_url($thumb[0]); ?>)">
                
                <div class="cases__wrap flex flex--center flex--justify flex--wrap">
                  <header>
                    <h2 class="cases__title--meta yellow h4">                   
                        <?php the_title(); ?>
                    </h2>

                    <h2 itemprop="headline" class="cases__title h3"><?php echo esc_html($title); ?></h2>

                    <?php 
                      //echo categories as tags
                      foreach ($categories as $category) : 

                      if ($category->cat_ID === 4) {
                        $color_class = 'btn--hollow-tag--red';
                      }

                      elseif ($category->cat_ID === 6) {
                        $color_class = 'btn--hollow-tag--bluelight';
                      }

                      elseif ($category->cat_ID === 8) {
                        $color_class = 'btn--hollow-tag--yellow';
                      }

                      elseif ($category->cat_ID === 5) {
                        $color_class = 'btn--hollow-tag--green';
                      }

                      else {
                        $color_class = 'btn--hollow-tag--white';
                      }
                    ?>

                        <span class="btn btn--tag btn--hollow-tag <?php echo esc_attr($color_class); ?>"><?php echo $category->name; ?></span>
                        
                    <?php 
                      endforeach; 
                    ?>
                  </header>
                </div>

              </a>

            <?php endwhile; wp_reset_postdata(); else: ?>
              
              <p>Cases er ikke tilgængelige på nuværende tidspunkt.</p>

          <?php endif; ?>

      </div>
    </div>
  </section>