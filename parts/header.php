<!doctype html>

<html <?php language_attributes(); ?>>

<head>
  <meta charset="utf-8">
  <title><?php wp_title( '|', true, 'right' ); ?></title>
  <?php wp_head(); ?>
</head>

<body <?php body_class(); ?> itemscope itemtype="https://schema.org/WebPage">

<header class="header" id="header" itemscope itemtype="http://schema.org/WPHeader">
  <div class="wrap--fluid hpad flex flex--center flex--justify header__container">

    <a class="header__logo" aria-label="logo" href="<?php bloginfo('url'); ?>">
      <?php echo file_get_contents('wp-content/themes/lionlab/assets/img/logo.svg'); ?> 
    </a>

    <nav class="nav" role="navigation" itemscope itemtype="http://schema.org/SiteNavigationElement">
      <div class="nav--mobile">
        <?php lionlab_main_nav(); ?>
      </div>
    </nav>

    <?php lionlab_cta_nav(); ?>

    <div class="nav-toggle"> 
      <span class="nav-toggle__label"><?php _e('Menu', 'lionlab') ?></span> 
      <span class="nav-toggle__icon"></span>
    </div>

  </div>
</header>

<div id="transition" class="transition">

<div class="transition__container" data-namespace="general"> 