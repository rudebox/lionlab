<?php 
	//contact informations
	$phone 	   = get_field('phone', 'options');
	$mail  	   = get_field('mail', 'options');
	$cvr   	   = get_field('cvr', 'options');
	$address   = get_field('address', 'options');

	//social links
	$fb = get_field('fb', 'options');
	$ig = get_field('ig', 'options');
	$li = get_field('li', 'options');
 ?>

<footer class="footer bg--black" id="footer" itemscope itemtype="http://schema.org/WPFooter">
	<div class="wrap hpad">
		<div class="row flex flex--wrap">

			<div class="col-sm-3 footer__column footer__column--contact">
				<h3 class="footer__title">Kontakt</h3>
				<a href="mailto:<?php echo esc_attr($mail); ?>"><?php echo esc_html($mail); ?></a><br>
				<a href="tel:<?php echo get_formatted_phone($phone); ?>"><?php echo esc_html($phone); ?></a>
				<br>
				<p>CVR: <?php echo esc_html($cvr); ?></p>
				<?php echo $address; ?>
			</div>

			<div class="col-sm-3 footer__column footer__column--social">
				<h3 class="footer__title">Social</h3>
				<a class="footer__social-link footer__social-link--fb" target="_blank" href="<?php echo esc_url($fb); ?>"><i class="fab fa-facebook-f"></i></a></br>
				<a class="footer__social-link footer__social-link--ig" target="_blank" href="<?php echo esc_url($ig); ?>"><i class="fab fa-instagram"></i></a></br>
				<a class="footer__social-link footer__social-link--li" target="_blank" href="<?php echo esc_url($li); ?>"><i class="fab fa-linkedin-in"></i></a>
			</div>

			<?php 
				if (have_rows('footer_columns', 'options') ) : while (have_rows('footer_columns', 'options') ) :
					the_row();
				$title = get_sub_field('column_title');
				$text = get_sub_field('column_text');
			 ?>

			 <div class="col-sm-3 footer__column">
			 	<?php if ($title) : ?>
			 	<h3 class="footer__title"><?php echo esc_html($title); ?></h3>
			 	<?php endif; ?>
				<?php echo $text; ?>
			 </div>

			<?php endwhile; endif; ?>

		</div>
	</div>
</footer>
<div id="modal"></div>
<div id="overlay"></div>

<?php wp_footer(); ?>
</div>
</div>

</body>
</html>