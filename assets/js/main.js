$(document).ready(function() {

  function init() {

    //in viewport check  
    var $animation_elements = $('.is-animated');
    var $window = $(window);

    function check_if_in_view() {
      var window_height = $window.height();
      var window_top_position = $window.scrollTop();
      var window_bottom_position = (window_top_position + window_height);
     
      $.each($animation_elements, function() {
        var $element = $(this);
        var element_height = $element.outerHeight();
        var element_top_position = $element.offset().top;
        var element_bottom_position = (element_top_position + element_height);
     
        //check to see if this current container is within viewport
        if ((element_top_position <= window_bottom_position)) {
          $element.addClass('in-view');
        } else {
          $element.removeClass('in-view');
        }
      });
    }

    $window.on('scroll resize', check_if_in_view);
    $window.trigger('scroll');
    

    //filter animation  
    $('.blog__filter--dropdown').on('click', function() {
      // $('.blog__filter').toggleClass('is-active');
      $(this).toggleClass('is-active'); 
      $('.blog__filter').each(function(index) {        
          var delayNumber = index * 100;
          
          $(this).delay(delayNumber).queue(function(next) {
            $(this).toggleClass('is-active');
            next();
          });  

      })
    }); 


    //menu toggle
    $('.nav-toggle').click(function(e) {
      e.preventDefault();
      $('.nav--mobile').toggleClass('is-open');
      $('body').toggleClass('is-fixed');
    });


    //masonry
    $('.masonry').masonry({

    });


    $('.nav__btn').on('click', function(e) {
      e.preventDefault();
      $('#modal').toggleClass('is-visible');
      $('#overlay').toggleClass('is-visible');
    });


    //mixit up
    if($('body').is('.blog')) {

        // Instantiate and configure the mixer
        var mixer = mixitup('.mixit', {
          
        });
    }

  }

  init();
  

  //
  // Barba.js
  //
  Barba.Pjax.Dom.wrapperId = 'transition';
  Barba.Pjax.Dom.containerClass = 'transition__container';
  Barba.Pjax.ignoreClassLink = 'no-ajax';

  var general = Barba.BaseView.extend({
    namespace: 'general',
    onEnter: function() {
      // The new Container is ready and attached to the DOM.
    },
    onEnterCompleted: function() {
      // The Transition has just finished.
      $('.header__logo').removeClass('is-active');
      $('html').removeClass('is-loading');
    },
    onLeave: function() {
      // A new Transition toward a new page has just started.
      $('.header__logo').addClass('is-active');
      $('html').addClass('is-loading'); 

    },
    onLeaveCompleted: function() {
      // The Container has just been removed from the DOM.

    }
  });


  general.init();

  Barba.Pjax.start();

  Barba.Prefetch.init(); 

  Barba.Pjax.originalPreventCheck = Barba.Pjax.preventCheck;

  Barba.Pjax.preventCheck = function(evt, element) {
    if (!Barba.Pjax.originalPreventCheck(evt, element)) {
      return false;
    }

    //outcomment if you want to prevent barba when logged in
    // if ($('body').hasClass('logged-in')) { Barba.Pjax.preventCheck = function() { 
    //   return false;  
    // }; } 

    // No need to check for element.href -
    // originalPreventCheck does this for us! (and more!)
    if (/.pdf/.test(element.href.toLowerCase())) {
      return false;
    } else if (/edit/.test(element.href.toLowerCase())) {
      return false;
    } else if (/wp-admin/.test(element.href.toLowerCase())) {
      return false;
    }

    return true;
  };

  var CommonTransition = Barba.BaseTransition.extend({
  start: function() {
  /**
  * This function is automatically called as soon the Transition starts
  * this.newContainerLoading is a Promise for the loading of the new container
  * (Barba.js also comes with an handy Promise polyfill!)
  */
  // As soon the loading is finished and the old page is faded out, let's fade the new page
  Promise
  .all([this.newContainerLoading, this.fadeOut()])
  .then(this.fadeIn.bind(this));
  },
  fadeOut: function() { 

  /**
  * this.oldContainer is the HTMLElement of the old Container
  */
  //return $(this.oldContainer).animate({ opacity: 0 }).promise();
  return $(this.oldContainer).animate({ opacity: 1}).promise();
  },
  fadeIn: function() {
    /**
    * this.newContainer is the HTMLElement of the new Container
    * At this stage newContainer is on the DOM (inside our #barba-container and with visibility: hidden)
    * Please note, newContainer is available just after newContainerLoading is resolved!
    */
    var _this = this;
    var $newContainer = $(this.newContainer);

    $(this.oldContainer).hide();
    $('html, body').animate({ scrollTop: 0 }, 400); // scroll to top here

    $newContainer.css({
      visibility: 'visible',
      opacity: 1
    });


    setTimeout(function () {    
      
      $newContainer.animate({
        opacity: 1,
      }, 400, function() {

        _this.done();

      });

    }, 400);
    }

  });
  /**
  * Next step, you have to tell Barba to use the new Transition
  */
  Barba.Pjax.getTransition = function() {
  /**
  * Here you can use your own logic!
  * For example you can use different Transition based on the current page or link...
  */
  return CommonTransition;

  };

  Barba.Dispatcher.on('initStateChange', function() {

    init();
    $('body').removeClass('is-fixed');
    $('.nav--mobile').removeClass('is-open');
    $('.nav__dropdown').removeClass('is-visible');

  });

  /**
  * GET WP body classes
  */
  Barba.Dispatcher.on('newPageReady', function(currentStatus, oldStatus, container, newPageRawHTML) {

    // html head parser borrowed from jquery pjax
    var $newPageHead = $( '<head />' ).html(
        $.parseHTML(
            newPageRawHTML.match(/<head[^>]*>([\s\S.]*)<\/head>/i)[0]
          , document
          , true
        )
    );

    var headTags = [
        "meta[name='keywords']"
      , "meta[name='description']"
      , "meta[property^='og']"
      , "meta[name^='twitter']"
      , "meta[itemprop]"
      , "link[itemprop]"
      , "link[rel='prev']"
      , "link[rel='next']"
      , "link[rel='canonical']"
    ].join(',');
    $( 'head' ).find( headTags ).remove(); // Remove current head tags
    $newPageHead.find( headTags ).appendTo( 'head' ); // Append new tags to the head 


    //update body classes
    var response = newPageRawHTML.replace(/(<\/?)body( .+?)?>/gi, '$1notbody$2>', newPageRawHTML)
    var bodyClasses = $(response).filter('notbody').attr('class')
    $('body').attr('class', bodyClasses);

    //get current url and path
    var $path = currentStatus.url.split(window.location.origin)[1].substring(1); 
    var $url = currentStatus.url;

    //add active class based upon URL status
    $('.nav__item a').each(function() {  
      if ($url === this.href) {
        $(this).addClass('is-active');
      }

      else {
        $(this).removeClass('is-active');
        $('.nav__item').removeClass('is-active');
      }
    });

    init();

  }); 

  Barba.Dispatcher.on('transitionCompleted', function (currentStatus, oldStatus, container) {
    
  });

});