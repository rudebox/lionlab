<?php get_template_part('parts/header'); the_post(); ?>

<main>

  <?php get_template_part('parts/page', 'header');?>

  <?php   
    //post bg
    $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'url' ); 

    //field
    $service = get_field('case_service');
    $date = get_field('case_date');
    $website = get_field('case_website');
  ?>

  <div class="page__img-container wrap hpad">
    <div class="page__img--wrapper">
      <div class="page__img" style="background-image: url(<?php echo esc_url($thumb[0]); ?>);"></div>
    </div>

    <div class="single-case__meta">

        <div class="single-case__meta-item">
          <p class="single-case__meta-desc">Kunde</p>
          <?php echo the_title(); ?>
        </div>

        <div class="single-case__meta-item">
          <p class="single-case__meta-desc">Service</p>
          <?php echo esc_html($service); ?>
        </div>

        <div class="single-case__meta-item">
          <p class="single-case__meta-desc">Periode</p>
          <?php echo esc_html($date); ?>
        </div>

        <div class="single-case__meta-item">
          <p class="single-case__meta-desc">Website</p>
          <a target="blank" rel="noopener" href="<?php echo esc_url($website); ?>"><?php echo $website; ?></a>
        </div>

    </div>
  </div>

  <section class="single-case padding--bottom">
    <div class="wrap hpad">
      <div class="row">

        <article class="col-sm-8 col-sm-offset-2" itemscope itemtype="http://schema.org/BlogPosting">

          <div itemprop="articleBody">
            <?php the_content(); ?>
          </div>

        </article>

      </div>
    </div>
  </section>

  <?php 
    if (have_rows('case_results') ) : 
  ?>
  <section class="single-case padding--bottom">
    <div class="wrap hpad">
      <div class="row">

        <div class="single-case__results col-sm-8 col-sm-offset-2">
          <h2>Resultater</h2>

          <div class="row">

            <?php 
                while (have_rows('case_results') ) : the_row();  

                $result = get_sub_field('case_result');
                $result_text = get_sub_field('case_result_text');
            ?>

              <div class="single-case__result col-sm-4">
                <p class="h2"><?php echo esc_html($result); ?></p>
                <?php echo esc_html($result_text); ?>
              </div>
            
            <?php endwhile; ?>

          </div>

        </div>

      </div>
    </div>
  </section>
  <?php endif; ?>

  <?php get_template_part('parts/cases'); ?>

  <?php get_template_part('parts/contact'); ?>

</main>

<?php get_template_part('parts/footer'); ?>