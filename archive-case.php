<?php get_template_part('parts/header'); ?>

<main>

<?php get_template_part('parts/page', 'header'); ?>

  <section class="blog padding--bottom">
    <div class="wrap hpad">
      <div class="row mixit flex flex--wrap">

        <?php if (have_posts()): ?>
          <?php while (have_posts()): the_post(); ?>

            <?php   
              //post img
              $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'url' ); 

              //get categories
              $categories = get_the_category();

              $title = get_field('page_title');
            ?>

            <a class="cases__item" href="<?php the_permalink(); ?>" itemscope itemtype="http://schema.org/BlogPosting" style="background-image: url(<?php echo esc_url($thumb[0]); ?>)">
              
                <header class="cases__header">
                  <h2 class="cases__title--meta yellow h4">                   
                      <?php the_title(); ?>
                  </h2>

                  <h2 itemprop="headline" class="cases__title h3"><?php echo esc_html($title); ?></h2>

                  <?php 
                    //echo categories as tags
                    foreach ($categories as $category) : 

                    if ($category->cat_ID === 4) {
                      $color_class = 'btn--hollow-tag--red';
                    }

                    elseif ($category->cat_ID === 6) {
                      $color_class = 'btn--hollow-tag--bluelight';
                    }

                    elseif ($category->cat_ID === 8) {
                      $color_class = 'btn--hollow-tag--yellow';
                    }

                    elseif ($category->cat_ID === 5) {
                      $color_class = 'btn--hollow-tag--green';
                    }

                    else {
                      $color_class = 'btn--hollow-tag--white';
                    }
                  ?>

               
                      
                  <?php 
                    endforeach; 
                  ?>
                </header>

            </a>

          <?php endwhile; else: ?>

            <p>Ingen indlæg i skoene eller i denne kategori for den sags skyld.</p>

        <?php endif; ?>

      </div>

      <?php 
        do_action( 'lionlab_pagination' );
      ?>
    </div>
  </section>

  <?php get_template_part('parts/contact'); ?>

</main>

<?php get_template_part('parts/footer'); ?>